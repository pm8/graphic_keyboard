/**! @license common.js by YukiTANABE@PM8.JP | MIT License | PM8.jp */
var JP_PM8_GRAFKEY=JP_PM8_GRAFKEY||{};
JP_PM8_GRAFKEY.COMMON={
    variables:{
        oct:[
            {tone:'C', kind:'whole', order:0},
            {tone:'C#', kind:'semi', order:1},
            {tone:'D', kind:'whole', order:2},
            {tone:'D#', kind:'semi', order:3},
            {tone:'E', kind:'whole', order:4},
            {tone:'F', kind:'whole', order:5},
            {tone:'F#', kind:'semi', order:6},
            {tone:'G', kind:'whole', order:7},
            {tone:'G#', kind:'semi', order:8},
            {tone:'A', kind:'whole', order:9},
            {tone:'A#', kind:'semi', order:10},
            {tone:'B', kind:'whole', order:11},
            ],
        cpm:[],
        base:{tone:'A', num:4, cpm:440},
        range:{
            from:{tone:'C',num:-1},
            to:{tone:'A',num:9}
        },
        isPlaying:false,
        ctx:new AudioContext()
    },
    elems:{
        keyboard:document.getElementById('keyboard')
    },
    funs:{
        init:()=>{
            JP_PM8_GRAFKEY.COMMON.funs.setup();
            JP_PM8_GRAFKEY.OUTPUT.funs.init();
            JP_PM8_GRAFKEY.INPUT.funs.init();
            JP_PM8_GRAFKEY.COMMON.funs.render();
        },
        setup:()=>{
            JP_PM8_GRAFKEY.COMMON.oscillator=JP_PM8_GRAFKEY.COMMON.variables.ctx.createOscillator();
            let nn=JP_PM8_GRAFKEY.COMMON.variables.oct.find(tone=>{
                return tone.tone===JP_PM8_GRAFKEY.COMMON.variables.base.tone;
            });
            let ff=JP_PM8_GRAFKEY.COMMON.variables.oct.find(tone=>{
                return tone.tone===JP_PM8_GRAFKEY.COMMON.variables.range.from.tone;
            });
            let tt=JP_PM8_GRAFKEY.COMMON.variables.oct.find(tone=>{
                return tone.tone===JP_PM8_GRAFKEY.COMMON.variables.range.to.tone;
            });
            nn=nn.order;
            ff=ff.order;
            tt=tt.order;
            for(let n=JP_PM8_GRAFKEY.COMMON.variables.range.from.num;n<=JP_PM8_GRAFKEY.COMMON.variables.range.to.num; n++){
                let m=[];
                let a=JP_PM8_GRAFKEY.COMMON.variables.base.cpm*Math.pow(2,n-JP_PM8_GRAFKEY.COMMON.variables.base.num);
                let c=a*Math.pow(2,-nn/JP_PM8_GRAFKEY.COMMON.variables.oct.length)
                for(let t=0; t<JP_PM8_GRAFKEY.COMMON.variables.oct.length; t++){
                    if(JP_PM8_GRAFKEY.COMMON.variables.range.from.num===n){
                        if(JP_PM8_GRAFKEY.COMMON.variables.oct[t].order<ff) continue;
                    }else if(n===JP_PM8_GRAFKEY.COMMON.variables.range.to.num){
                        if(JP_PM8_GRAFKEY.COMMON.variables.oct[t].order>tt) continue;
                    }
                    let mm={};
                    mm.tone=JP_PM8_GRAFKEY.COMMON.variables.oct[t].tone;
                    mm.kind=JP_PM8_GRAFKEY.COMMON.variables.oct[t].kind;
                    mm.number=(JP_PM8_GRAFKEY.COMMON.variables.oct.length*(n+1)+t).toString(16);
                    mm.cmp=c*Math.pow(2,t/JP_PM8_GRAFKEY.COMMON.variables.oct.length);
                    m.push(mm)
                }
                JP_PM8_GRAFKEY.COMMON.variables.cpm.push(m)
            }
        },
        render:()=>{
            for (let i=0; i<JP_PM8_GRAFKEY.COMMON.variables.cpm.length; i++){
                for (let t=0; t<JP_PM8_GRAFKEY.COMMON.variables.cpm[i].length;t++){
                    let key=document.createElement('li');
                    key.classList.add(JP_PM8_GRAFKEY.COMMON.variables.cpm[i][t].kind);
                    key.setAttribute('data-cmp', JP_PM8_GRAFKEY.COMMON.variables.cpm[i][t].cmp);
                    key.setAttribute('data-number', JP_PM8_GRAFKEY.COMMON.variables.cpm[i][t].number);
                    let txt =JP_PM8_GRAFKEY.COMMON.variables.cpm[i][t].tone;
                    if(t===0) txt+=String(i+JP_PM8_GRAFKEY.COMMON.variables.range.from.num);
                    key.appendChild(document.createTextNode(txt));
                    JP_PM8_GRAFKEY.COMMON.elems.keyboard.appendChild(key);
                }
            }
        }
    }
};